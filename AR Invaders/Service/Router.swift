//
//  Router.swift
//  AR Invaders
//
//  Created by Richmond Ko on 19/07/2018.
//  Copyright © 2018 aivantgoyal. All rights reserved.
//

import Foundation
import Alamofire


enum Router: URLRequestConvertible {
    
    static let baseURLString = "https://api.telegram.org/bot658353752:AAHurhhGzCVvrDzKYZY4-t0-Hi8QlEbbNn4"
    static let chatId = "-304120519"
    case sendMessage(text: String)
    
    private var method: HTTPMethod {
        switch self {
        case .sendMessage: return .post
        }
    }
    
    private var path: String {
        switch self {
        case .sendMessage:
            return "sendMessage"
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Router.baseURLString.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .sendMessage(let text):
            let parameters: [String: Any] = [
                "chat_id": Router.chatId,
                "text": text
            ]
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }
        return urlRequest
    }
    
}
