//
//  TelegramBotService.swift
//  AR Invaders
//
//  Created by Richmond Ko on 19/07/2018.
//  Copyright © 2018 aivantgoyal. All rights reserved.
//

import Foundation
import Alamofire

final class TelegramBotService {
    private init() {}
    static let shared = TelegramBotService()
    
    func sendMessage(text: String, callback: @escaping(Bool, Error?) -> Void) {
        Alamofire.request(Router.sendMessage(text: text)).response { (response) in
            callback(true, nil)
        }
    }
}
